#!/usr/bin/python3
multiples = []

for i in range(1, int(1000003/3)):
   if i *3 < 1000000:
      multiples.append(i*3)
   else:
      break


for i in range(1, int(1000007/7)):
   if i *7 < 1000000:
      multiples.append(i*7)
   else:
      break

multiples_set = set(multiples)
print("The multiple set has "+str(len(multiples_set))+" Numbers in it")

print(sum(multiples_set))
